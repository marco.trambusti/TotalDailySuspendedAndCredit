# Creazione di un nuovo task
$hour    = Read-Host "Inserisci l'ora di esecuizione dell'invio dei resoconti"
$phpPath = Read-Host "Inserisci il perchorso completo a php.exe (es. C:\Path\To\php.exe)"

# Ottieni il percorso dello script corrente
$scriptPath = Split-Path -Parent $MyInvocation.MyCommand.Definition
$relativePath = ".\artisan"
# Combina il percorso dello script con il percorso relativo
$combinedPath = Join-Path -Path $scriptPath -ChildPath $relativePath

# Ottieni il percorso assoluto
$absolutePath = Resolve-Path $combinedPath

$Action = New-ScheduledTaskAction -Execute $phpPath -Argument "$absolutePath app:send-summary}"
$Trigger = New-ScheduledTaskTrigger -Daily -At $hour
$Settings = New-ScheduledTaskSettingsSet -StartWhenAvailable -RestartInterval (New-TimeSpan -Minutes 10) -RestartCount 3
$Principal = New-ScheduledTaskPrincipal -UserId "NT AUTHORITY\SYSTEM" -LogonType ServiceAccount -RunLevel Highest
$Task = New-ScheduledTask -Action $Action -Trigger $Trigger -Settings $Settings -Principal $Principal
Register-ScheduledTask -TaskName "SendSummary" -InputObject $Task