<?php

namespace App\Console\Commands;

use App\Mail\TotalSpentAndCreditOfTheDay;
use App\Models\Caparra;
use App\Models\Conto;
use Illuminate\Console\Command;
use DateTime;
use Exception;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

use function Laravel\Prompts\error;

class SendSummary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:send-summary';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $summary = [];

    const TYPE_CREDITO = 'credito';
    const TYPE_SOSPESI = 'sospesi';
    private $caparreDiscr = [];

    private function initSummaryItem($data) {

        $this->summary[$data->id_anagrafica] = [
            "nome"    => $data->nome,
            "cognome" => $data->cognome,
            "email"   => 'badaw85173@hdrlog.com', //$data->email,
            "sospesi" => [
                "totale" => 0,
                'voceContoNuovi' => [],
            ],
            "credito"  => [
                "totale" => -1,
                'voceContoNuovi' => [],
            ],
            "daySpent" => (new DateTime())->format("d/m/Y")
        ];
    }

    private function  InsertComponentInSummary($obj, $type) {

        if(!isset($this->summary[$obj->id_anagrafica])) {

            $this->initSummaryItem($obj);
        }

        if($type == $this::TYPE_CREDITO) {
            
            if($this->summary[$obj->id_anagrafica][$type]["totale"] == -1) {
                $this->summary[$obj->id_anagrafica][$type]["totale"] = 0;
            }
    
            if(!in_array($obj->id_caparra, $this->caparreDiscr)) {
    
                $this->summary[$obj->id_anagrafica][$type]["totale"] += $obj->credito;
                $this->caparreDiscr[] = $obj->id_caparra;
            }

        } else if ($type == $this::TYPE_SOSPESI){
            $this->summary[$obj->id_anagrafica][$type]["totale"] += (float) $obj->totale_di_riga;
        } else {
            throw new Exception("INVALID TYPE");
        }

        if(isset($obj->data_chiusura) && new DateTime($obj->data_chiusura) >= new DateTime('today midnight')) {

            if(!isset($this->summary[$obj->id_anagrafica][$type]["VoceContoNuovi"][$obj->id_conto])) {
                $this->summary[$obj->id_anagrafica][$type]["VoceContoNuovi"][$obj->id_conto] = [
                    "Totale" => $obj->Totale,
                    "righe"  => [],
                ];
            }

            $this->summary[$obj->id_anagrafica][$type]["VoceContoNuovi"][$obj->id_conto]["righe"][] = [
                "descrizione"  =>  $obj->vc_descrizione,
                "quantita"     =>  $obj->vc_quantita,
                "totaleDiRiga" =>  $obj->totale_di_riga,
            ];
        } 
    }
    /**
     * Execute the console command.
     */
    public function handle()
    {
        $sospesi = Conto::getSospesi();
        $crediti = Caparra::getCredito();

        foreach($sospesi as $sospeso) {

            $this->InsertComponentInSummary($sospeso, $this::TYPE_SOSPESI);
            // if(!isset($this->summary[$sospeso->id_anagrafica])) {

            //     $this->initSummaryItem($sospeso);
            // }


            // if(new DateTime($sospeso->data_chiusura) >= new DateTime('today midnight')) {

            //     if(!isset($this->summary[$sospeso->id_anagrafica]["sospesi"]["VoceContoNuoviSospesi"][$sospeso->id_conto])) {
            //         $this->summary[$sospeso->id_anagrafica]["sospesi"]["VoceContoNuoviSospesi"][$sospeso->id_conto] = [
            //             "Totale" => $sospeso->Totale,
            //             "righe"  => [],
            //         ];
            //     }

            //     $this->summary[$sospeso->id_anagrafica]["sospesi"]["VoceContoNuoviSospesi"][$sospeso->id_conto]["righe"][] = [
            //         "descrizione"  =>  $sospeso->vc_descrizione,
            //         "quantita"     =>  $sospeso->vc_quantita,
            //         "totaleDiRiga" =>  $sospeso->totale_di_riga,
            //     ];
            // } 
        }

        foreach($crediti as $credito) {

            $this->InsertComponentInSummary($credito, $this::TYPE_CREDITO);
            // if(!isset($this->summary[$credito->id_anagrafica])) {

            //     $this->initSummaryItem($credito);
            // }

            // if($this->summary[$credito->id_anagrafica]["credito"]["totaleCredito"] == -1) {
            //     $this->summary[$credito->id_anagrafica]["credito"]["totaleCredito"] = 0;
            // }

            // if(!in_array($credito->id_caparra, $this->caparreDiscr)) {

            //     $this->summary[$credito->id_anagrafica]["credito"]["totaleCredito"] += $credito->credito;
            //     $this->caparreDiscr[] = $credito->id_caparra;
            // }

            // if(new DateTime($credito->data_chiusura) >= new DateTime('today midnight')) {

            //     if(!isset($this->summary[$credito->id_anagrafica]["credito"]["VoceContoNuoviCredito"][$credito->id_conto])) {
            //         $this->summary[$credito->id_anagrafica]["credito"]["VoceContoNuoviCredito"][$credito->id_conto] = [
            //             "Totale" => $credito->Totale_pagato,
            //             "righe"  => [],
            //         ];
            //     }

            //     $this->summary[$credito->id_anagrafica]["credito"]["VoceContoNuoviCredito"][$credito->id_conto]["righe"][] = [
            //         "descrizione"  =>  $credito->vc_descrizione,
            //         "quantita"     =>  $credito->vc_quantita,
            //         "totaleDiRiga" =>  $credito->totale_di_riga,
            //     ];
            // } 
        }
//error_log(print_r($this->summary, true));

        foreach($this->summary as $s) {

            //sleep(5);
            
            $validator = Validator::make($s, ['email' => 'email:rfc'],);

            if(empty($s["credito"]["VoceContoNuovi"]) && empty($s["sospesi"]["VoceContoNuovi"])){
                continue;
            }
            
            if($validator->fails()) {
                error_log($validator->messages().'email is :'.$s["email"]);
                continue;
            }
            
            Mail::to($s["email"])->send(new TotalSpentAndCreditOfTheDay($s));
        }

    }
}
