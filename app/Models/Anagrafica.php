<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Anagrafica extends Model
{
    use HasFactory;

    const COL_NOME    = 'Nome';
    const COL_COGNOME = 'Cognome';
    const COL_EMAIL   = 'Email';
    const COL_ID      = 'Id';

    const TABLE_NAME ='Anagrafica';

    /**
     * Nome della tabella
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * PrimaryKey del Modello
     * @var string
     */
    protected $primaryKey = self::COL_ID;

    /**
     * Tipo della PrimaryKey
     * @var string
     */
    protected $keyType = 'integer';
}
