<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use DateTime;

class Caparra extends Model
{
    use HasFactory;

    const COL_IDCLIENTE      = 'IdCliente';
    const COL_UTLIMAMODIFICA = 'dataUltimaModifica';
    const COL_IMPORTO        = 'Importo';
    const COL_UTILIZZATO     = 'utilizzo';
    const COL_ID             = 'Id';

    const TABLE_NAME ='Caparra';

    /**
     * Nome della tabella
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * PrimaryKey del Modello
     * @var string
     */
    protected $primaryKey = self::COL_ID;

    /**
     * Tipo della PrimaryKey
     * @var string
     */
    protected $keyType = 'integer';

    public static function getCredito() {

        $anagraficaTable = Anagrafica::TABLE_NAME;
        $contoTable      = Conto::TABLE_NAME;
        $caparraTable    = static::TABLE_NAME;
        $clienteTable    = Cliente::TABLE_NAME;
        $voceContoTable  = VoceConto::TABLE_NAME;
        $caparraXpagTable = CaparraXPag::TABLE_NAME;
        $pagamentoTable = Pagamento::TABLE_NAME;

        $idAnagrafica         = $anagraficaTable.'.'.Anagrafica::COL_ID;
        $nome                 = $anagraficaTable.'.'.Anagrafica::COL_NOME;
        $cognome              = $anagraficaTable.'.'.Anagrafica::COL_COGNOME;
        $email                = $anagraficaTable.'.'.Anagrafica::COL_EMAIL;
        
        $idCliente            = $clienteTable.'.'.Cliente::COL_ID;
        $idCaparra            = $caparraTable.'.'.static::COL_ID;
        $idAnagraficaCliente  = $clienteTable.'.'.Cliente::COL_IDANAGRAFICA;
        $importo              = $caparraTable.'.'.static::COL_IMPORTO;
        $utilizzato           = $caparraTable.'.'.static::COL_UTILIZZATO;
        $idClienteCaparra     = $caparraTable.'.'.static::COL_IDCLIENTE;
        $idAnagraficaCliente  = $clienteTable.'.'.Cliente::COL_IDANAGRAFICA;
        $dataUltimaModifica   = $caparraTable.'.'.static::COL_UTLIMAMODIFICA;
        $descrizioneVoceConto = $voceContoTable.'.'.VoceConto::COL_DESCRIZIONE;
        $quantitaVoceConto    = $voceContoTable.'.'.VoceConto::COL_QUANTITA;
        $totaleDiRiga         = $voceContoTable.'.'.VoceConto::COL_TOTALEDIRIGA;
        $dataChiusura         = $contoTable.'.'.Conto::COL_DATACHIUSUSRA;
        $idCaparraCapxPag     = $caparraXpagTable.'.'.CaparraXPag::COL_IDCAPARRA;
        $idPagamentoCapxPag   = $caparraXpagTable.'.'.CaparraXPag::COL_IDPAGAMENTO;
        $idPagamento          = $pagamentoTable.'.'.Pagamento::COL_ID;
        $idConto              = $contoTable.'.'.Conto::COL_ID;
        $idContoPagamento     = $pagamentoTable.'.'.Pagamento::COL_IDCONTO;
        $idContoVoceconto     = $voceContoTable.'.'.VoceConto::COL_IDCONTO;
        $dataAnnullo          = $contoTable.'.'.Conto::COL_DATAANNULLO;
        $pagato               = $contoTable.'.'.Conto::COL_PAGATO;

        $query = "SELECT {$idAnagrafica} as id_anagrafica, {$idClienteCaparra} as id_cliente, {$dataUltimaModifica} as ultima_modifica, {$nome} as nome, {$cognome} as cognome, {$email} as email, ({$importo} - {$utilizzato}) as credito, {$descrizioneVoceConto} as vc_descrizione, {$quantitaVoceConto} as vc_quantita, {$totaleDiRiga} as totale_di_riga, {$dataChiusura} as data_chiusura, {$pagato} as Totale, {$idConto} as id_conto, {$idCaparra} as id_caparra
        FROM {$caparraTable}
        JOIN {$clienteTable} ON {$idClienteCaparra} = {$idCliente}
        JOIN {$anagraficaTable} ON {$idAnagraficaCliente} = {$idAnagrafica}
        JOIN {$caparraXpagTable} ON {$idCaparra} = {$idCaparraCapxPag}
        JOIN {$pagamentoTable} ON {$idPagamentoCapxPag} = {$idPagamento}
        JOIN {$contoTable} ON {$idContoPagamento} = {$idConto}
        JOIN {$voceContoTable} ON {$idConto}={$idContoVoceconto}
        WHERE {$email} IS NOT NULL and {$email} != '' AND {$dataAnnullo} IS NULL --and {$dataUltimaModifica} >= CONVERT(datetime2, ?, 121) --AND ({$importo} - {$utilizzato}) != 0 ";
//error_log(print_r([$query,(new DateTime('today midnight'))->format("Y-m-d H:i:s.v")], true));

        return DB::connection('sqlsrv')->select($query, [(new DateTime('today midnight'))->format("Y-m-d H:i:s.v")]);
    }
}
