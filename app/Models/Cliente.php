<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use HasFactory;

    const COL_ID = 'Id';
    const COL_IDANAGRAFICA = 'IdAnagrafica';

    const TABLE_NAME ='Cliente';

    /**
     * Nome della tabella
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * PrimaryKey del Modello
     * @var string
     */
    protected $primaryKey = self::COL_ID;

    /**
     * Tipo della PrimaryKey
     * @var string
     */
    protected $keyType = 'integer';

}
