<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Conto extends Model
{
    use HasFactory;

    const COL_IDCLIENTE     = 'IdCliente';
    const COL_DATAANNULLO   = 'dataAnnullo';
    const COL_DATACREAZIONE = 'dataCreazione';
    const COL_DATACHIUSUSRA = 'dataChiusura';
    const COL_SOSPESO       = 'Sospeso';
    const COL_PAGATO        = 'totalePagato';
    const COL_ID            = 'Id';
    const COL_STATO         = 'stato';

    const TABLE_NAME ='Conto';

    /**
     * Nome della tabella
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * PrimaryKey del Modello
     * @var string
     */
    protected $primaryKey = self::COL_ID;

    /**
     * Tipo della PrimaryKey
     * @var string
     */
    protected $keyType = 'integer';

    public static function getSospesi() {
        $contoTable      = static::TABLE_NAME;
        $voceContoTable  = VoceConto::TABLE_NAME;
        $clienteTable    = Cliente::TABLE_NAME;
        $anagraficaTable = Anagrafica::TABLE_NAME;

        $idAnagrafica         = $anagraficaTable.'.'.Anagrafica::COL_ID;
        $nome                 = $anagraficaTable.'.'.Anagrafica::COL_NOME;
        $cognome              = $anagraficaTable.'.'.Anagrafica::COL_COGNOME;
        $email                = $anagraficaTable.'.'.Anagrafica::COL_EMAIL;
        $idCliente            = $clienteTable.'.'.Cliente::COL_ID;
        $idConto              = $contoTable.'.'.static::COL_ID;
        $dataAnnullo          = $contoTable.'.'.static::COL_DATAANNULLO;
        $dataCreazione        = $contoTable.'.'.static::COL_DATACREAZIONE;
        $dataChiusura         = $contoTable.'.'.static::COL_DATACHIUSUSRA;
        $sospeso              = $contoTable.'.'.static::COL_SOSPESO;
        $descrizioneVoceConto = $voceContoTable.'.'.VoceConto::COL_DESCRIZIONE;
        $quantitaVoceConto    = $voceContoTable.'.'.VoceConto::COL_QUANTITA;
        $totaleDiRiga         = $voceContoTable.'.'.VoceConto::COL_TOTALEDIRIGA;

        $idContoVoceconto     = $voceContoTable.'.'.VoceConto::COL_IDCONTO;
        $idClienteConto       = $contoTable.'.'.static::COL_IDCLIENTE;
        $idAnagraficaCliente  = $clienteTable.'.'.Cliente::COL_IDANAGRAFICA;
        $statoConto           = $contoTable.'.'.static::COL_STATO;
        
        $query ="SELECT {$idAnagrafica} as id_anagrafica, {$idCliente} as id_cliente, {$idConto} as id_conto , {$dataAnnullo} as data_annullo, {$dataCreazione} as data_creazione, {$dataChiusura} as data_chiusura, {$nome} as nome, {$cognome} as cognome, {$email} as email, {$descrizioneVoceConto} as vc_descrizione, {$quantitaVoceConto} as vc_quantita, {$totaleDiRiga} as totale_di_riga, {$sospeso} as Totale
        FROM {$contoTable}
        JOIN {$voceContoTable} ON {$idConto}={$idContoVoceconto}
        JOIN {$clienteTable} ON {$idClienteConto} = {$idCliente}
        JOIN {$anagraficaTable} ON {$idAnagraficaCliente} = {$idAnagrafica}
        WHERE {$statoConto} = 2 AND {$sospeso} !=0 AND {$dataAnnullo} IS NULL and {$email} IS NOT NULL and {$email} != ''
        ORDER BY {$dataChiusura} DESC";
//error_log(print_r($query, true));

        return DB::connection('sqlsrv')->select($query);
    }
}
