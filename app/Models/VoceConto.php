<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VoceConto extends Model
{
    use HasFactory;

    const COL_DESCRIZIONE  = 'Descrizione';
    const COL_QUANTITA     = 'Quantita';
    const COL_TOTALEDIRIGA = 'TotaleDiRiga';
    const COL_ID           = 'Id';
    const COL_IDCONTO      = 'IdConto';

    const TABLE_NAME ='VoceConto';

    /**
     * Nome della tabella
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * PrimaryKey del Modello
     * @var string
     */
    protected $primaryKey = self::COL_ID;

    /**
     * Tipo della PrimaryKey
     * @var string
     */
    protected $keyType = 'integer';
}
