﻿<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>
</head>
<body>
    <pre>
{{config('app.nomeLocale')}}<br/>
<br />
{{config('app.intro')}}<br />
del giorno {{$summary['daySpent']}}<br />
del Cliente {{$summary['nome']}} {{$summary['cognome']}}<br />
                       <br />
La presente mail viene inviata in automatico come rendiconto quotidiano<br />
dei servizi e/o delle consumazioni di bar e ristorante da Voi fruiti in data odierna<br />
@if($summary['sospesi']['totale'])
e che il nostro sistema amministrativo ha caricato sul Vostro conto in modalità “conti sospesi”,<br />
tramite l'emissione di uno scontrino a incasso zero (corrispettivo da incassare).<br />
                       <br />
Totale complessivo dei sospesi: Euro {{number_format($summary['sospesi']['totale'],2)}}<br />
@if(count($summary['sospesi']['VoceContoNuovi']) > 0)
Ecco i dettagli dei sospesi odierni:<br />
@foreach ($summary['sospesi']['VoceContoNuovi'] as $conto)
    @foreach ($conto['righe'] as $voceConto)
     {{$voceConto['quantita']}} {{$voceConto['descrizione']}} - Euro {{$voceConto['totaleDiRiga'] ? number_format($voceConto['totaleDiRiga'],2):0.00}}<br />
    @endforeach
@endforeach
@endif
@endif
@if($summary['credito']['totale'] > -1)
<br />
il Vostro credito residuo aggiornato è: Euro {{number_format($summary['credito']['totale'], 2)}} <br/>
@if(count($summary['credito']['VoceContoNuovi']) > 0)
Ecco i dettagli dei conti odierni per cui è stato utilizzato il credito:<br />
@foreach ($summary['credito']['VoceContoNuovi'] as $conto)
    @foreach ($conto['righe'] as $voceConto)
     {{$voceConto['quantita']}} {{$voceConto['descrizione']}} - Euro {{$voceConto['totaleDiRiga'] ? number_format($voceConto['totaleDiRiga'],2):0.00}}<br />
    @endforeach
@endforeach
@endif
@endif
<br />
Qualora Lei rilevasse delle difformità o necessitasse di qualche chiarimento, La preghiamo di rivolgersi tempestivamente<br />
agli addetti di Cassa del Bagno oppure scrivere al presente <a href="{{config('app.mailReply')}}">indirizzo email</a>.<br />
                                                  <br />
Cordiali saluti e buon proseguimento di vacanza,<br />
{{config('app.nomeLocale')}}<br />
<br />
____________________________<br />
{{config('app.nomeLocale')}}<br/>
<br />
{{config('app.intro')}}<br />
Day: {{$summary['daySpent']}}<br />
Client: {{$summary['nome']}} {{$summary['cognome']}}<br />
                       <br />
Dear Customer,<br />
This email is automatically sent as a daily statement <br />
of the bar and restaurant services and/or consumption enjoyed by you today<br />
@if($summary['sospesi']['totale'])
and that our administrative system has charged onto your account
<br />
Overall total of unpaid: Euro {{number_format($summary['sospesi']['totale'],2)}}<br />
@if(count($summary['sospesi']['VoceContoNuovi']) > 0)
Details of today's unpaid services:<br />
@foreach ($summary['sospesi']['VoceContoNuovi'] as $conto)
    @foreach ($conto['righe'] as $voceConto)
     {{$voceConto['quantita']}} {{$voceConto['descrizione']}} - Euro {{$voceConto['totaleDiRiga'] ? number_format($voceConto['totaleDiRiga'],2) :0.00}}<br />
    @endforeach
@endforeach
@endif
@endif
@if($summary['credito']['totale'] > -1)
<br />
Your updated remaining credit is: Euro {{number_format($summary['credito']['totale'],2)}} <br/>
@if(count($summary['credito']['VoceContoNuovi']) > 0)
Details of today's accounts for which the credit was used:<br />
@foreach ($summary['credito']['VoceContoNuovi'] as $conto)
    @foreach ($conto['righe'] as $voceConto)
     {{$voceConto['quantita']}} {{$voceConto['descrizione']}} - Euro {{$voceConto['totaleDiRiga'] ? number_format($voceConto['totaleDiRiga'],2):0.00}}<br />
    @endforeach
@endforeach
@endif
@endif
                                                  <br />
If you find any discrepancies or need any clarification, please contact our staff at Bagno Piero or send an email to this <a href="{{config('app.mailReply')}}">address</a>.<br />
                                                  <br />
Best regards,<br />
{{config('app.nomeLocale')}}<br />
</pre>
</body>
</html>