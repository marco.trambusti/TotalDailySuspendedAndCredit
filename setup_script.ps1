#powershell -ExecutionPolicy ByPass -file
# Chiedi all'utente i valori di configurazione
$dbHost     = Read-Host "Inserisci l'host del database"
$dbDatabase = Read-Host "Inserisci il nome del database"
$dbUsername = Read-Host "Inserisci l'username del database"
$dbPassword = Read-Host "Inserisci la password del database" -AsSecureString

$dbPasswordClear = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($dbPassword))

$mailHost        = Read-Host "Inserisci l'host della mail"
$mailPort        = Read-Host "Inserisci la porta della mail"
$mailUsername    = Read-Host "Inserisci l'username della mail"
$mailPassword    = Read-Host "Inserisci la password della mail" -AsSecureString
$mailFromAddress = Read-Host "Inserisci l'indirizzo mail mittente"

$mailPasswordClear = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($mailPassword))

$nomeLocale = Read-Host "Inserisci il nome del locale"
$mailReply  = Read-Host "Inserisci lindirizzo mail a cui il cliente potrebbe inviare una risposta"

$samplePath = ".\.env.example"
$envPath = ".\.env"
$confAppPath = ".\config\app.php"

# Controlla se il file .env esiste
if (Test-Path $envPath) {
    Write-Host "Il file .env esiste già."
} else {
    # Copia il file .env.sample in .env
    Copy-Item -Path $samplePath -Destination $envPath
    Write-Host "File .env creato copiando .env.sample."
}

# Leggi il contenuto del file .env
$content = Get-Content $envPath

# Modifica le variabili nel file .env
$content = $content -replace 'DB_HOST=.*', "DB_HOST=$dbHost"
$content = $content -replace 'DB_PORT=.*', "#DB_PORT=3306"
$content = $content -replace 'DB_DATABASE=.*', "DB_DATABASE=$dbDatabase"
$content = $content -replace 'DB_USERNAME=.*', "DB_USERNAME=$dbUsername"
$content = $content -replace 'DB_PASSWORD=.*', "DB_PASSWORD=$dbPasswordClear"

$content = $content -replace 'MAIL_HOST=.*', "MAIL_HOST=$mailHost"
$content = $content -replace 'MAIL_PORT=.*', "MAIL_PORT=$mailPort"
$content = $content -replace 'MAIL_USERNAME=.*', "MAIL_USERNAME='$mailUsername'"
$content = $content -replace 'MAIL_PASSWORD=.*', "MAIL_PASSWORD='$mailPasswordClear'"
$content = $content -replace 'MAIL_FROM_ADDRESS=.*', "MAIL_FROM_ADDRESS='$mailFromAddress'"

# Scrivi il contenuto modificato nel file .env
$content | Set-Content $envPath

Write-Host "Le variabili nel file .env sono state modificate con successo."

if(Test-Path $confAppPath) {

     # Leggi il contenuto del file app.php
    $content = Get-Content $confAppPath

    $content = $content -replace "'nomeLocale' =>.*", "'nomeLocale' =>'$nomeLocale',"
    $content = $content -replace "'mailReply' =>.*", "'mailReply' =>'mailto:$mailReply',"

    # Scrivi il contenuto modificato nel file .env
    $content | Set-Content $confAppPath

    Write-Host "Le variabili nel file app.php sono state modificate con successo."
} else {
    Write-Host "Il file app.php non esiste!"
}

& ".\add_planned_Activity.ps1"